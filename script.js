/*
    JMP Numbers That Are Blocked or Not
    Copyright (C) 2022  Maxime V.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const url = "output.json"

const months = ["Jan.", "Feb.", "Mar.", "Apr.", "May", "Jun.", "Jul.", "Aug.", "Sep.", "Oct.", "Nov.", "Dec."];

let services = []

function loadPage() {
    fetch(url)
        .then(response => response.json())
        .then(data => {
            data.forEach((item) => {
                services.push(item["Service"])
                html = '';
                html += `
                <div class="dropdown-item">
                    <a id="id-${cleanString(item["Service"])}"></a>
                    `

                    if (item["AdditionalComments"] != null) {
                        html += `
                            <div class="additionalComments">
                                <span class="material-symbols-outlined">tips_and_updates</span>
                                <span class="additionalCommentText"><b>Note: </b>${item["AdditionalComments"]}</span>     
                            </div>
                            
                        `
                    }
    
                    html +=`
                    <details id="details-${cleanString(item["Service"])}" class="detailsService">
                        <summary class="dropdown-title">${item["Service"]}</summary>
                        
                        <div>
                            <table style="width: 100%;">
                                <tr>
                                    <td width="33%"></td>
                                    <td width="33%">✔️</td>
                                    <td width="33%">❌</td>
                                </tr>
                                <tr>
                                    <th>Sign-up</th>
                                    <td>${CorrectReturnNull(item["SignUpSuccess"])}</td>
                                    <td>${CorrectReturnNull(item["SignUpFailure"])}</td>
                                </tr>
                                <tr>
                                    <th>2FA</th>
                                    <td>${CorrectReturnNull(item["2faSuccess"])}</td>
                                    <td>${CorrectReturnNull(item["2faFailure"])}</td>
                                </tr>
                                <tr>
                                    <th>Last update</th>
                                    <td>${formatDate(item["LastSuccess"])}</td>
                                    <td>${formatDate(item["LastFailure"])}</td>
                                </tr>
                            </table>
                        </div>
                    </details>
                </div>`
                
                document.getElementById('services').innerHTML += html
            })
            window.location.href = "?#!"
            loadSearch()
            showLastEdited('./last-edited')
        })
}

// https://stackoverflow.com/a/14446538
function showLastEdited(file) {
    fetch(file)
        .then(response => response.text())
        .then(text => document.getElementById('lastEdited').innerHTML += "Last fetched: " + text) 
}

function loadSearch() {
    let options = '';
    services.forEach(function (item) {
        options += `<option value="${item}" />`;
    })
    document.getElementById('searchListData').innerHTML = options;
}

function moveToService() {
    let service = document.getElementById("searchBar").value
    service = cleanString(service)

    if (service == "") {
        document.getElementById("searchBar").style.border = "2px solid red"
        document.getElementById("searchError").innerHTML = "Search field cannot be empty";
    } else {
        location.replace(`#id-${service}`)
        document.getElementById(`details-${service}`).open = true;
        document.getElementById("searchError").innerHTML = "";
    }
}

// returns 0 if null/empty/etc...
function CorrectReturnNull(fieldToCheck) {
    if ([" ", null, 0, " 0"].includes(fieldToCheck)) {
        return "N/A";
    }
    else return fieldToCheck
}

// removes all whitespace from a string
function removeSpaces(fieldToClean) {
    return fieldToClean.replace(/\s/g, '_')
}

// removes all dots from a string
function removeDots(fieldToClean) {
    return fieldToClean.replace(/\./g, '_dot_')
}

// removes all parenthesis from a string
function removeParenthesis(fieldToClean) {
    return fieldToClean.replace(/[{()}]/g, '-')
}

function cleanString(toClean) {
    toClean = removeSpaces(toClean)
    toClean = removeDots(toClean)
    toClean = removeParenthesis(toClean)
    return toClean.toLowerCase()
}

function formatDate(dateInput) {
    let dateRaw = CorrectReturnNull(dateInput)
    dateRaw = dateRaw.toString().replace(/\s/g, '')

    // fix for chromium when date == 0, it converts to january 1st, 2000
    if (dateRaw == "N/A") {
        return "N/A"
    }

    let date = new Date(dateRaw)

    const month = months[date.getMonth()];
    const year = date.getFullYear();

    const finalDate = `${month} ${year}`
    return finalDate
}
